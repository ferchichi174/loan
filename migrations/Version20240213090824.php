<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240213090824 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer ADD residency_status VARCHAR(255) DEFAULT NULL, ADD income_type VARCHAR(255) DEFAULT NULL, ADD monthly_income VARCHAR(255) DEFAULT NULL, ADD direct_deposit VARCHAR(255) DEFAULT NULL, ADD account_type VARCHAR(255) DEFAULT NULL, ADD credit_card_debt_amount VARCHAR(255) DEFAULT NULL, ADD auto_owner_status VARCHAR(255) DEFAULT NULL, ADD months_at_bank VARCHAR(255) DEFAULT NULL, ADD months_at_address VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer DROP residency_status, DROP income_type, DROP monthly_income, DROP direct_deposit, DROP account_type, DROP credit_card_debt_amount, DROP auto_owner_status, DROP months_at_bank, DROP months_at_address');
    }
}
