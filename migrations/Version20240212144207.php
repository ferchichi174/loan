<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240212144207 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer ADD zipcode VARCHAR(255) DEFAULT NULL, ADD street VARCHAR(255) NOT NULL, ADD city VARCHAR(255) NOT NULL, ADD state VARCHAR(255) NOT NULL, ADD primaryphone VARCHAR(255) NOT NULL, ADD dob VARCHAR(255) NOT NULL, ADD employername VARCHAR(255) NOT NULL, ADD employerphone VARCHAR(255) NOT NULL, ADD jobtitle VARCHAR(255) NOT NULL, ADD phonesub1 VARCHAR(255) NOT NULL, ADD paydate1 VARCHAR(255) NOT NULL, ADD ssn VARCHAR(255) NOT NULL, ADD licence VARCHAR(255) NOT NULL, ADD dlstate VARCHAR(255) NOT NULL, ADD routingnumber VARCHAR(255) NOT NULL, ADD accountnumber VARCHAR(255) NOT NULL, ADD bankname VARCHAR(255) NOT NULL, ADD firstname VARCHAR(255) NOT NULL, ADD lastname VARCHAR(255) NOT NULL, ADD active_military VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer DROP zipcode, DROP street, DROP city, DROP state, DROP primaryphone, DROP dob, DROP employername, DROP employerphone, DROP jobtitle, DROP phonesub1, DROP paydate1, DROP ssn, DROP licence, DROP dlstate, DROP routingnumber, DROP accountnumber, DROP bankname, DROP firstname, DROP lastname, DROP active_military');
    }
}
