<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240214150428 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer ADD loan_reason VARCHAR(255) NOT NULL, ADD domain VARCHAR(255) NOT NULL, ADD subid VARCHAR(255) NOT NULL, ADD credit_score VARCHAR(255) NOT NULL, ADD pay_date2 VARCHAR(255) NOT NULL, ADD middlename VARCHAR(255) DEFAULT NULL, ADD cid VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer DROP loan_reason, DROP domain, DROP subid, DROP credit_score, DROP pay_date2, DROP middlename, DROP cid');
    }
}
