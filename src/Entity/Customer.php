<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\UX\Turbo\Attribute\Broadcast;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]
#[Broadcast]
class Customer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $zipcode = null;

    #[ORM\Column(length: 255)]
    private ?string $street = null;

    #[ORM\Column(length: 255)]
    private ?string $city = null;

    #[ORM\Column(length: 255)]
    private ?string $state = null;

    #[ORM\Column(length: 255)]
    private ?string $primaryphone = null;

    #[ORM\Column(length: 255)]
    private ?string $dob = null;

    #[ORM\Column(length: 255)]
    private ?string $employername = null;

    #[ORM\Column(length: 255)]
    private ?string $employerphone = null;

    #[ORM\Column(length: 255)]
    private ?string $jobtitle = null;

    #[ORM\Column(length: 255)]
    private ?string $phonesub1 = null;

    #[ORM\Column(length: 255)]
    private ?string $paydate1 = null;

    #[ORM\Column(length: 255)]
    private ?string $ssn = null;

    #[ORM\Column(length: 255)]
    private ?string $licence = null;

    #[ORM\Column(length: 255)]
    private ?string $dlstate = null;

    #[ORM\Column(length: 255)]
    private ?string $routingnumber = null;

    #[ORM\Column(length: 255)]
    private ?string $accountnumber = null;

    #[ORM\Column(length: 255)]
    private ?string $bankname = null;

    #[ORM\Column(length: 255)]
    private ?string $firstname = null;

    #[ORM\Column(length: 255)]
    private ?string $lastname = null;

    #[ORM\Column(length: 255)]
    private ?string $activeMilitary = null;

    #[ORM\Column(length: 255)]
    private ?string $ip = null;

    #[ORM\Column(length: 255)]
    private ?string $userAgent = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $residencyStatus = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $incomeType = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $monthlyIncome = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $directDeposit = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $accountType = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $creditCardDebtAmount = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $autoOwnerStatus = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $monthsAtBank = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $monthsAtAddress = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $requestedAmount = null;

    #[ORM\Column(length: 255)]
    private ?string $loanReason = null;

    #[ORM\Column(length: 255)]
    private ?string $domain = null;

    #[ORM\Column(length: 255)]
    private ?string $subid = null;

    #[ORM\Column(length: 255)]
    private ?string $creditScore = null;

    #[ORM\Column(length: 255)]
    private ?string $payDate2 = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $middlename = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $cid = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(?string $zipcode): static
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): static
    {
        $this->street = $street;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): static
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): static
    {
        $this->state = $state;

        return $this;
    }

    public function getPrimaryphone(): ?string
    {
        return $this->primaryphone;
    }

    public function setPrimaryphone(string $primaryphone): static
    {
        $this->primaryphone = $primaryphone;

        return $this;
    }

    public function getDob(): ?string
    {
        return $this->dob;
    }

    public function setDob(string $dob): static
    {
        $this->dob = $dob;

        return $this;
    }

    public function getEmployername(): ?string
    {
        return $this->employername;
    }

    public function setEmployername(string $employername): static
    {
        $this->employername = $employername;

        return $this;
    }

    public function getEmployerphone(): ?string
    {
        return $this->employerphone;
    }

    public function setEmployerphone(string $employerphone): static
    {
        $this->employerphone = $employerphone;

        return $this;
    }

    public function getJobtitle(): ?string
    {
        return $this->jobtitle;
    }

    public function setJobtitle(string $jobtitle): static
    {
        $this->jobtitle = $jobtitle;

        return $this;
    }

    public function getPhonesub1(): ?string
    {
        return $this->phonesub1;
    }

    public function setPhonesub1(string $phonesub1): static
    {
        $this->phonesub1 = $phonesub1;

        return $this;
    }

    public function getPaydate1(): ?string
    {
        return $this->paydate1;
    }

    public function setPaydate1(string $paydate1): static
    {
        $this->paydate1 = $paydate1;

        return $this;
    }

    public function getSsn(): ?string
    {
        return $this->ssn;
    }

    public function setSsn(string $ssn): static
    {
        $this->ssn = $ssn;

        return $this;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setLicence(string $licence): static
    {
        $this->licence = $licence;

        return $this;
    }

    public function getDlstate(): ?string
    {
        return $this->dlstate;
    }

    public function setDlstate(string $dlstate): static
    {
        $this->dlstate = $dlstate;

        return $this;
    }

    public function getRoutingnumber(): ?string
    {
        return $this->routingnumber;
    }

    public function setRoutingnumber(string $routingnumber): static
    {
        $this->routingnumber = $routingnumber;

        return $this;
    }

    public function getAccountnumber(): ?string
    {
        return $this->accountnumber;
    }

    public function setAccountnumber(string $accountnumber): static
    {
        $this->accountnumber = $accountnumber;

        return $this;
    }

    public function getBankname(): ?string
    {
        return $this->bankname;
    }

    public function setBankname(string $bankname): static
    {
        $this->bankname = $bankname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getActiveMilitary(): ?string
    {
        return $this->activeMilitary;
    }

    public function setActiveMilitary(string $activeMilitary): static
    {
        $this->activeMilitary = $activeMilitary;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): static
    {
        $this->ip = $ip;

        return $this;
    }

    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    public function setUserAgent(string $userAgent): static
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    public function getResidencyStatus(): ?string
    {
        return $this->residencyStatus;
    }

    public function setResidencyStatus(?string $residencyStatus): static
    {
        $this->residencyStatus = $residencyStatus;

        return $this;
    }

    public function getIncomeType(): ?string
    {
        return $this->incomeType;
    }

    public function setIncomeType(?string $incomeType): static
    {
        $this->incomeType = $incomeType;

        return $this;
    }

    public function getMonthlyIncome(): ?string
    {
        return $this->monthlyIncome;
    }

    public function setMonthlyIncome(?string $monthlyIncome): static
    {
        $this->monthlyIncome = $monthlyIncome;

        return $this;
    }

    public function getDirectDeposit(): ?string
    {
        return $this->directDeposit;
    }

    public function setDirectDeposit(?string $directDeposit): static
    {
        $this->directDeposit = $directDeposit;

        return $this;
    }

    public function getAccountType(): ?string
    {
        return $this->accountType;
    }

    public function setAccountType(?string $accountType): static
    {
        $this->accountType = $accountType;

        return $this;
    }

    public function getCreditCardDebtAmount(): ?string
    {
        return $this->creditCardDebtAmount;
    }

    public function setCreditCardDebtAmount(?string $creditCardDebtAmount): static
    {
        $this->creditCardDebtAmount = $creditCardDebtAmount;

        return $this;
    }

    public function getAutoOwnerStatus(): ?string
    {
        return $this->autoOwnerStatus;
    }

    public function setAutoOwnerStatus(?string $autoOwnerStatus): static
    {
        $this->autoOwnerStatus = $autoOwnerStatus;

        return $this;
    }

    public function getMonthsAtBank(): ?string
    {
        return $this->monthsAtBank;
    }

    public function setMonthsAtBank(?string $monthsAtBank): static
    {
        $this->monthsAtBank = $monthsAtBank;

        return $this;
    }

    public function getMonthsAtAddress(): ?string
    {
        return $this->monthsAtAddress;
    }

    public function setMonthsAtAddress(?string $monthsAtAddress): static
    {
        $this->monthsAtAddress = $monthsAtAddress;

        return $this;
    }

    public function getRequestedAmount(): ?string
    {
        return $this->requestedAmount;
    }

    public function setRequestedAmount(?string $requestedAmount): static
    {
        $this->requestedAmount = $requestedAmount;

        return $this;
    }

    public function getLoanReason(): ?string
    {
        return $this->loanReason;
    }

    public function setLoanReason(string $loanReason): static
    {
        $this->loanReason = $loanReason;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): static
    {
        $this->domain = $domain;

        return $this;
    }

    public function getSubid(): ?string
    {
        return $this->subid;
    }

    public function setSubid(string $subid): static
    {
        $this->subid = $subid;

        return $this;
    }

    public function getCreditScore(): ?string
    {
        return $this->creditScore;
    }

    public function setCreditScore(string $creditScore): static
    {
        $this->creditScore = $creditScore;

        return $this;
    }

    public function getPayDate2(): ?string
    {
        return $this->payDate2;
    }

    public function setPayDate2(string $payDate2): static
    {
        $this->payDate2 = $payDate2;

        return $this;
    }

    public function getMiddlename(): ?string
    {
        return $this->middlename;
    }

    public function setMiddlename(?string $middlename): static
    {
        $this->middlename = $middlename;

        return $this;
    }

    public function getCid(): ?string
    {
        return $this->cid;
    }

    public function setCid(?string $cid): static
    {
        $this->cid = $cid;

        return $this;
    }
}
