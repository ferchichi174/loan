<?php

namespace App\Controller;

use App\Entity\Customer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CustomerController extends AbstractController
{
    #[Route('/api/customer', name: 'app_customer',methods: ['POST'])]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $requestData = json_decode($request->getContent(), true);
        $email = $requestData['email'];
        $accountNumber = $requestData['accountNumber'];
        $bankName = $requestData['bankName'];
        $city = $requestData['city'];
        $dlState = $requestData['dlState'];
        $dob = $requestData['dob'];
        $employerName = $requestData['employerName'];
        $employerPhone = $requestData['employerPhone'];
        $firstName = $requestData['firstName'];
        $lastName = $requestData['lastName'];
        $jobtitle = $requestData['jobtitle'];
        $licence = $requestData['licence'];
        $militir = $requestData['militir'];
        $payDate1 = $requestData['payDate1'];
        $phonesub1 = $requestData['phonesub1'];
        $primaryPhone = $requestData['primaryPhone'];
        $routingNumber = $requestData['routingNumber'];
        $ssn = $requestData['ssn'];
        $state = $requestData['state'];
        $street = $requestData['street'];
        $zipCode = $requestData['zipCode'];
        $ip = $requestData['ipAddress'];
        $userAgent = $requestData['userAgent'];
        $residencyStatus = $requestData['residencyStatus'];
        $incomeType = $requestData['incomeType'];
        $monthlyIncome = $requestData['monthlyIncome'];
        $directDeposit = $requestData['directDeposit'];
        $accountType = $requestData['accountType'];
        $creditCardDebtAmount = $requestData['creditCardDebtAmount'];
        $autoOwnerStatus = $requestData['autoOwnerStatus'];
        $monthsAtBank = $requestData['monthsAtBank'];
        $monthsAtAddress = $requestData['monthsAtAddress'];
        $requestedAmount = $requestData['requestedAmount'];
        $loanReason = $requestData['loanReason'];
        $domain = $requestData['domain'];
        $subid = $requestData['subid'];
        $cid = $requestData['cid'];
        $creditScore = $requestData['creditScore'];
        $payDate2 = $requestData['payDate2'];
        $middlename = $requestData['middlename'];

        $customer = new Customer();
        $customer->setFirstname($firstName);
        $customer->setLastname($lastName);
        $customer->setEmail($email);
        $customer->setAccountnumber($accountNumber);
        $customer->setBankname($bankName);
        $customer->setCity($city);
        $customer->setDlstate($dlState);
        $customer->setDob($dob);
        $customer->setEmployername($employerName);
        $customer->setEmployerphone($employerPhone);
        $customer->setJobtitle($jobtitle);
        $customer->setLicence($licence);
        $customer->setActiveMilitary($militir);
        $customer->setPaydate1($payDate1);
        $customer->setPhonesub1($phonesub1);
        $customer->setPrimaryphone($primaryPhone);
        $customer->setRoutingnumber($routingNumber);
        $customer->setSsn($ssn);
        $customer->setState($state);
        $customer->setStreet($street);
        $customer->setZipcode($zipCode);
        $customer->setIp($ip);
        $customer->setUserAgent($userAgent);
        $customer->setResidencyStatus($residencyStatus);
        $customer->setIncomeType($incomeType);
        $customer->setMonthlyIncome($monthlyIncome);
        $customer->setDirectDeposit($directDeposit);
        $customer->setAccountType($accountType);
        $customer->setCreditCardDebtAmount($creditCardDebtAmount);
        $customer->setAutoOwnerStatus($autoOwnerStatus);
        $customer->setMonthsAtBank($monthsAtBank);
        $customer->setMonthsAtAddress($monthsAtAddress);
        $customer->setRequestedAmount($requestedAmount);
        $customer->setLoanReason($loanReason);
        $customer->setDomain($domain);
        $customer->setSubid($subid);
        $customer->setCid($cid);
        $customer->setCreditScore($creditScore);
        $customer->setPayDate2($payDate2);
        $customer->setMiddlename($middlename);
        $entityManager->persist($customer);
        $entityManager->flush();
        $response = [
            'status' => 'success',
            'message' => 'Données reçues avec succès'
        ];

        // Convertir la réponse en JSON
        return $this->json($response);
    }
}
